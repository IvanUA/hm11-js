const passwordForm = document.querySelector(".password-form");
const button = document.querySelector(".btn");

passwordForm.addEventListener('click', (event) => {

    if (event.target.classList.contains("fas")) {
        const inputWrapper = event.target.closest("label");
        const input = inputWrapper.querySelector("input");
        if (event.target.classList.contains("fa-eye-slash")) {
            event.target.classList.remove("fa-eye-slash")
            input.type = "text"
        } else {
            event.target.classList.add("fa-eye-slash")
            input.type = "password"
        }
    }
});

button.addEventListener('click', () => {
    const password1 = document.getElementById('input-first').value;
    const password2 = document.getElementById('input-check').value;
    const mistake = document.createElement('p');

    if (password1 === password2) {
        alert("You are welcome");
    } else {
        document.body.appendChild(mistake);
        mistake.style.color = 'red';
        mistake.textContent = 'Потрібно ввести однакові значення';
    }
});

 
